//
//  ViewController.swift
//  AVAudioPCMBufferExample
//
//  Created by Aaron Wangugi on 9/10/18.
//  Copyright © 2018 Aaron Wangugi. All rights reserved.
//

import UIKit
import AudioKit

class ViewController: UIViewController {
    var micMixer: AKMixer!
    var recorder: AKNodeRecorder!
    var tracker: AKAmplitudeTracker!
    var silence: AKBooster!
    var player: AKPlayer!
    var tape: AKAudioFile!
    var moogLadder: AKMoogLadder!
    var micBooster: AKBooster!
    var delay: AKDelay!
    var mainMixer: AKMixer!
    let mix = AKMicrophone()
    var globalAudioFile: AKAudioFile?
    
    enum State {
        case readyToRecord
        case recording
        case readyToPlay
        case playing
    }
    
    var state = State.readyToRecord
    
    func setSettings() {
        AKSettings.bufferLength = .longest
        AKSettings.sampleRate = 48000
        do {
            try AKSettings.setSession(category: .playAndRecord, with: .allowBluetoothA2DP)
        } catch {
            AKLog("Could not set session category.")
        }
        
        AKSettings.defaultToSpeaker = true
    }
    
    // Mark: User action handling
    
    @objc func handleRecord() {
        if AKSettings.headPhonesPlugged {
            micBooster.gain = 1
        }

        if state == State.readyToRecord {
            do {
                try recorder.record()
            } catch {
                print("Errored during recording.")
            }
        }
    }
    
    @objc func handleStopRecord() {
        
        //handle audio
        state = State.readyToPlay
        micBooster.gain = 0
        tape = recorder.audioFile!
        player.load(audioFile: tape)
        player.buffering = .always
        recorder.stop()
        
        if let _ = player.audioFile?.duration {
            tape.exportAsynchronously(name: "TempTestfile.m4a", baseDir: .documents, exportFormat: .caf) { (_, exportError) in
                if let error = exportError {
                    print("Export Failed \(error)")
                } else {
                    print("Export Succeeded.")
                }
            }
        }
        
        if let avAudioPCMBuffer = player.buffer {
            
            do {
                globalAudioFile = try AKAudioFile()
            } catch let error as NSError {
                print("Recorder Error: Unable to open AudioFile: \(error.localizedDescription)")
            }
            
            do {
                try globalAudioFile?.write(from: avAudioPCMBuffer)
            } catch let error as NSError {
                print("Recorder Error: Unable to write buffer to AudioFile: \(error.localizedDescription).")
            }
            
            do {
                // We initialize AKAudioFile for writing (and check that we can write to)
                globalAudioFile = try AKAudioFile(forWriting: (globalAudioFile?.url)!,
                                                  settings: (globalAudioFile?.processingFormat.settings)!)
            } catch let error as NSError {
                print("Recorder Error: cannot write to \(String(describing: globalAudioFile?.fileNamePlusExtension))")
                print(error.localizedDescription)
            }
        }
        
        //update UI
        recordButton.removeFromSuperview()
        
        setupCancelButton()
        setupPlayButtons()
        
        self.view.addSubview(playButton)
    }
    
    @objc func handlePlay() {
        player.play()
    }
    
    @objc func handleBufferedPlay() {
        if let audioFile = globalAudioFile {
            let bufferedPlayer = audioFile.player
            bufferedPlayer.play()
        }
    }
    
    @objc func handleCancel() {
        //delete sound
        player.stop()
        do {
            try recorder.reset()
        } catch {
            print("Errored resetting.")
        }
        
        //update UI
        removeCancelButton()
        playButton.removeFromSuperview()
        bufferedPlayButton.removeFromSuperview()
        //        loopButton.setImage(#imageLiteral(resourceName: "Loop").withRenderingMode(.alwaysOriginal), for: .normal)
        //        loopButton.removeFromSuperview()
        self.view.addSubview(recordButton)
        placeButton(button: recordButton, superView: self.view)
        state = .readyToRecord
    }
    
    // Mark: UI Manipulation
    
    func setupRecordButton() {
        view.addSubview(recordButton)
        placeButton(button: recordButton, superView: self.view)
    }
    
    func placeButton(button: UIButton, superView: UIView) {
        button.anchor(top: nil, left: nil, bottom: superView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 300, paddingRight: 0, width: 80, height: 80)
        button.centerXAnchor.constraint(equalTo: superView.centerXAnchor).isActive = true
    }

    fileprivate func setupCancelButton() {
        self.view.addSubview(cancelButton)
        cancelButton.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: nil, paddingTop: 45, paddingLeft: 45, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    fileprivate func setupPlayButtons() {
        self.view.addSubview(playButton)
        self.view.addSubview(bufferedPlayButton)

        let viewsDict = ["playButton": playButton, "bufferedPlayButton": bufferedPlayButton] as [String: Any]
        playButton.translatesAutoresizingMaskIntoConstraints = false
        bufferedPlayButton.translatesAutoresizingMaskIntoConstraints = false
        for subView in viewsDict.keys {
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat:"H:|-20-[\(subView)]-20-|", options: [], metrics: nil, views: viewsDict))
        }
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-200-[playButton(<=60)]-30-[bufferedPlayButton(<=60)]-300-|", options: [], metrics: nil, views: viewsDict))
    }
    
    fileprivate func removeCancelButton() {
        cancelButton.removeFromSuperview()
    }
    
    lazy var recordButton: UIButton = {
        let button = UIButton(type: .custom)
        
        button.addTarget(self, action: #selector(handleRecord), for: .touchDown)
        button.addTarget(self, action: #selector(handleStopRecord), for: .touchUpInside)
        return button
    }()
    
    lazy var playButton: UIButton = {
        var button = UIButton()
        button.backgroundColor = UIColor.white
        button.setTitle("Play", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 20
        button.layer.borderWidth = 5
        button.layer.borderColor = UIColor.black.cgColor
        button.addTarget(self, action: #selector(handlePlay), for: .touchUpInside)
        return button
    }()
    
    lazy var bufferedPlayButton: UIButton = {
        var button = UIButton()
        button.backgroundColor = UIColor.white
        button.setTitle("BufferedPlay", for: .normal)
        button.layer.cornerRadius = 20
        button.layer.borderWidth = 5
        button.layer.borderColor = UIColor.black.cgColor
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleBufferedPlay), for: .touchUpInside)
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "cancel_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return button
    }()
    
    // Mark: Business Logic
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = .all
        AKAudioFile.cleanTempDirectory()
        setSettings()
        
        micMixer = AKMixer(mix)
        micBooster = AKBooster(micMixer)
        tracker = AKAmplitudeTracker.init(mix, halfPowerPoint: 10, threshold: 1, thresholdCallback: { (_) in
        })
        
        silence = AKBooster(tracker, gain: 0)
        
        micBooster.gain = 0
        recorder = try? AKNodeRecorder(node: micMixer)
        recorder.durationToRecord = 5.0
        if let file = recorder.audioFile {
            player = AKPlayer(audioFile: file)
            player.completionHandler = {
                self.player.stop()
            }
        } else {
            print("Unable to initialize player.")
        }
        
        player.isLooping = false
        let booster2 = AKBooster(player)
        mainMixer = AKMixer(booster2, micBooster)
        
        
        AudioKit.output = mainMixer
        do {
            try AudioKit.start()
        } catch {
            AKLog("Audiokit did not start!")
        }
        
        view.backgroundColor = UIColor.white
        setupRecordButton()
        getPermissionForAudio()
    }

    func getPermissionForAudio() {
        AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
            if hasPermission
            {
                print ("Accepted")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AudioKit.output = silence
        do {
            try AudioKit.start()
        } catch {
            AKLog("Audiokit did not start.")
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        recordButton.setBackgroundImage(UIImage.circle(diameter: recordButton.bounds.size.width, color: UIColor.white).circularImageWithBorderOf(color: UIColor.black, diameter: recordButton.bounds.size.width, borderWidth: CGFloat(9.0)), for: .normal)
        recordButton.setBackgroundImage(UIImage.circle(diameter: recordButton.bounds.size.width, color: UIColor.black).circularImageWithBorderOf(color: UIColor.white, diameter: recordButton.bounds.size.width, borderWidth: CGFloat(9.0)), for: .selected)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// Mark: Helpers

extension UIView {
        func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
            
            translatesAutoresizingMaskIntoConstraints = false
            
            if let top = top {
                self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
            }
            
            if let left = left {
                self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
            }
            
            if let bottom = bottom {
                self.bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
            }
            
            if let right = right {
                self.rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
            }
            
            if width != 0 {
                widthAnchor.constraint(equalToConstant: width).isActive = true
            }
            
            if height != 0 {
                heightAnchor.constraint(equalToConstant: height).isActive = true
            }
    }

}


