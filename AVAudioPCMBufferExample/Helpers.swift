//
//  Helpers.swift
//  AVAudioPCMBufferExample
//
//  Created by Aaron Wangugi on 9/10/18.
//  Copyright © 2018 Aaron Wangugi. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func circularImageWithBorderOf(color: UIColor, diameter: CGFloat, borderWidth:CGFloat) -> UIImage {
        let aRect = CGRect.init(x: 0, y: 0, width: diameter, height: diameter)
        UIGraphicsBeginImageContextWithOptions(aRect.size, false, self.scale)
        
        color.setFill()
        UIBezierPath.init(ovalIn: aRect).fill()
        
        let anInteriorRect = CGRect.init(x: borderWidth, y: borderWidth, width: diameter-2*borderWidth, height: diameter-2*borderWidth)
        UIBezierPath.init(ovalIn: anInteriorRect).addClip()
        
        self.draw(in: anInteriorRect)
        
        let anImg = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return anImg
    }
    
    class func circle(diameter: CGFloat, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)
        let ctx = UIGraphicsGetCurrentContext()!
        ctx.saveGState()
        
        let rect = CGRect(x: 0, y: 0, width: diameter, height: diameter)
        ctx.setFillColor(color.cgColor)
        ctx.fillEllipse(in: rect)
        
        ctx.restoreGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return img
    }
}

class Helpers {
    
}
