A couple of notes:

1. Because I used cocoapods to install audiokit, before you open the workspace be sure to run 'pod install' from the directory in which you are working.
2. Press and hold the circle, record button in the middle and it will record for 5 seconds from the microphone. 
3. Works on both simulators and real devices, however if using a macbook with simulator will probably need to get closer to the microphones.
4. The play button will user AKPlayer to play, the bufferedPlay button will attempt to play using AVAudioPCMBuffer, but it will throw an exception and crash the project.
